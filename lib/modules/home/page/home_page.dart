import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import 'package:pokedex/core/layout/layout_controller.dart';
import 'package:pokedex/core/util/util_core.dart';
import 'package:pokedex/modules/home/home_module.dart';
import 'package:pokedex/modules/home/page/widgets/list_filter_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return LayoutController(
      body: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const ListFilterWidget(),
                BlocConsumer<HomeBloc, HomeState>(
                  listener: (context, state) => Logger.info.log(state),
                  builder: (context, state) {
                    if (state is HomeFetch) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } else if (state is HomeEmptyList) {
                      return const Center(
                        child: Text('List is empty'),
                      );
                    } else if (state is HomeLoadedList) {
                      List<Result> data = state.data.results;
                      return ListView.separated(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (context, index) => ListTile(
                          leading:
                              Text((data[index].id).toString().padLeft(3, '0')),
                          title: Text(data[index].name),
                          onTap: () => GoRouter.of(context)
                              .go('/pokemon/${data[index].id}'),
                        ),
                        separatorBuilder: (context, index) => const Divider(),
                        itemCount: data.length,
                      );
                    } else {
                      return const Center(
                        child: Text('Unexpected error'),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
