import 'package:pokedex/core/api/api_controller.dart';
import 'package:pokedex/core/util/util_core.dart';

void example() async {
  var api = ApiController(baseUrl: 'https://pokeapi.co/api/v2');
  var result = await api.request(path: 'pokemon/1');

  if (result is Success) {
    Success success = result;
    Logger.info.log('value ${success.value}');
  } else if (result is Failure) {
    Failure error = result;
    Logger.info.log('error ${error.error}');
  }
}
