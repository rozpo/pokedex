import 'package:dio/dio.dart';

class ApiController {
  static const String root = 'pokeapi.co';
  static const String version = '/api/v2';

  final String baseUrl;
  late Dio _client;

  ApiController({required this.baseUrl}) {
    final options = BaseOptions(
      baseUrl: baseUrl,
    );

    _client = Dio(options);
  }

  static Uri pokemon({int? offset, int? limit}) {
    Map<String, dynamic> queryParams = {};

    if (offset != null) {
      queryParams.putIfAbsent('offset', () => offset.toString());
    }

    if (limit != null) {
      queryParams.putIfAbsent('limit', () => limit.toString());
    }

    Uri uri = Uri.https(
      root,
      '$version/pokemon',
      queryParams.length > 1 ? queryParams : null,
    );

    return uri;
  }

  static Uri pokemonId({required int id}) {
    Uri uri = Uri.https(
      root,
      '$version/pokemon/$id',
    );

    return uri;
  }

    Future<Result<T>>? request<T>({
    required String path,
    Map<String, dynamic>? queryParameters,
    dynamic data,
    String? method,
    String? contentType,
  }) async {
    var response = await _client.request(
      '/$path',
      queryParameters: queryParameters,
      data: data,
      options: Options(
        method: method,
        contentType: contentType,
      ),
    );

    if (response.statusCode == 200) {
      return Success(response.data);
    } else {
      return Failure(Exception(response.data));
    }
  }
}

dynamic transform<T>(TransformCallback<T> transform) {
  return transform;
}

typedef TransformCallback<T> = T Function(dynamic data);

abstract class Result<T> {
  const Result();

  T? get value;
  Exception? get error;
}

class Success<T> extends Result<T> {
  final T _value;

  const Success(this._value);

  @override
  T? get value => _value;

  @override
  Exception? get error => null;
}

class Failure<T> extends Result<T> {
  final Exception _exception;

  const Failure(this._exception);

  @override
  T? get value => null;

  @override
  Exception? get error => _exception;
}
