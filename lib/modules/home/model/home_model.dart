class HomeModel {
  HomeModel({
    required this.count,
    this.next,
    this.previous,
    required this.results,
  });

  int count;
  Params? next;
  Params? previous;
  List<Result> results;

  HomeModel.fromJson(Map<String, dynamic> json)
      : count = json['count'],
        results = [] {
    if (json['previous'] != null) {
      Uri prevUri = Uri.parse(json['previous']);
      previous = Params(
        offset: prevUri.queryParameters['offset'],
        limit: prevUri.queryParameters['limit'],
      );
    }

    if (json['next'] != null) {
      Uri nextUri = Uri.parse(json['next'] as String);
      next = Params(
        offset: nextUri.queryParameters['offset'],
        limit: nextUri.queryParameters['limit'],
      );
    }

    for (var element in json['results']) {
      results.add(
        Result(
          name: element['name'],
          url: element['url'],
          id: int.tryParse(
                  Uri.parse(element['url']).pathSegments[
                      Uri.parse(element['url']).pathSegments.length - 2],
                  radix: 10) ??
              0,
        ),
      );
    }
  }
}

class Result {
  Result({
    required this.name,
    required this.url,
    required this.id,
  });

  String name;
  String url;
  int id;
}

class Params {
  Params({
    this.offset,
    this.limit,
  });

  String? offset;
  String? limit;
}
