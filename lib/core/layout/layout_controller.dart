import 'package:flutter/material.dart';

import 'package:pokedex/core/layout/src/layout_landscape.dart';
import 'package:pokedex/core/layout/src/layout_portrait.dart';

class LayoutController extends StatelessWidget {
  final Widget body;

  const LayoutController({
    super.key,
    required this.body,
  });

  static const _landscapeBreakpoint = 900;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return constraints.maxWidth > _landscapeBreakpoint
            ? LayoutLandscape(body: body)
            : LayoutPortrait(body: body);
      },
    );
  }
}
