import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_strategy/url_strategy.dart';

import 'package:pokedex/core/cache/cache_controller.dart';
import 'package:pokedex/core/locale/locale_controller.dart';
import 'package:pokedex/core/router/router_core.dart';
import 'package:pokedex/core/theme/theme_controller.dart';
import 'package:pokedex/core/util/util_core.dart';
import 'package:pokedex/modules/poke_info/poke_info_module.dart';

void main() {
  init();
  run();
}

void init() async {
  Logger.setDefaultLogLevel();
  setPathUrlStrategy();
  await CacheController.init();
}

void run() async {
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => PokeInfoBloc(),
        ),
      ],
      child: MaterialApp.router(
        title: Configer.appName,
        theme: ThemeController.themeData,
        routerConfig: RouterController.router,
        localizationsDelegates: LocaleController.localizationsDelegates,
        supportedLocales: LocaleController.supportedLocales,
      ),
    ),
  );
}
