part of 'poke_info_bloc.dart';

@immutable
abstract class PokeInfoEvent {}

class PokeInfoFetchEvent extends PokeInfoEvent {
  final int id;

  PokeInfoFetchEvent(this.id);
}
