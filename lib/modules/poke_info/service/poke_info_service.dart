import 'package:http/http.dart';

mixin PokeInfoService {
  static Future<Response> getPokemon(Uri uri) async {
    final Response response = await get(uri);

    return response;
  }
}
