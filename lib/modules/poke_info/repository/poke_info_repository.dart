import 'dart:convert';

import 'package:pokedex/core/api/api_controller.dart';
import 'package:pokedex/core/cache/cache_controller.dart';
import 'package:pokedex/core/util/util_core.dart';
import 'package:pokedex/modules/poke_info/model/poke_info_model.dart';

import '../service/poke_info_service.dart';

mixin PokeInfoRepository {
  static Future<PokeInfoModel?> fetchPokemon(int id) async {
    var uri = ApiController.pokemonId(id: id);
    String? request = CacheController.getRequest(uri.toString());

    if (request == null) {
      Logger.debug.log('no cache for endpoint $uri');
      final result = await PokeInfoService.getPokemon(uri);

      if (result.statusCode == 200) {
        Logger.debug.log('request status code: ${result.statusCode}');
        request = result.body;
        await CacheController.saveRequest(uri.toString(), request);
      } else {
        Logger.error.log('request error code: ${result.statusCode}');
      }
    } else {
      Logger.debug.log('using cache for endpoint $uri');
    }

    return PokeInfoModel.fromJson(jsonDecode(request!));
  }
}
