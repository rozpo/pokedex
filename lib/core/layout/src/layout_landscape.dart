import 'package:flutter/material.dart';

class LayoutLandscape extends StatelessWidget {
  final Widget body;

  const LayoutLandscape({
    super.key,
    required this.body,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Landscape'),
      ),
      body: body,
    );
  }
}
