import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokedex/modules/poke_info/bloc/poke_info_bloc.dart';
import 'package:pokedex/modules/poke_info/page/poke_info_page.dart';

class PokeInfoProvider extends StatelessWidget {
  final int id;

  const PokeInfoProvider({required this.id, super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      lazy: false,
      create: (context) => PokeInfoBloc()..add(PokeInfoFetchEvent(id)),
      child: const PokeInfoPage(),
    );
  }
}
