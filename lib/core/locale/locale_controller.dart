import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

mixin LocaleController {
  static AppLocalizations of(BuildContext context) {
    return AppLocalizations.of(context)!;
  }

  static List<Locale> supportedLocales = AppLocalizations.supportedLocales;
  static List<LocalizationsDelegate<dynamic>> localizationsDelegates =
      AppLocalizations.localizationsDelegates;
}
