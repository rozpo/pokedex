import 'package:flutter/material.dart';

class LayoutPortrait extends StatelessWidget {
  final Widget body;

  const LayoutPortrait({
    super.key,
    required this.body,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Portrait'),
      ),
      body: body,
    );
  }
}
