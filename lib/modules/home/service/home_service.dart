import 'package:http/http.dart';

mixin HomeService {
  static Future<Response> getPokemonList(Uri uri) async {
    final Response response = await get(uri);

    return response;
  }
}
