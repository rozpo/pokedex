import 'package:go_router/go_router.dart';

import 'package:pokedex/modules/error/error_module.dart';

import 'routes.dart';

mixin RouterController {
  static final router = GoRouter(
    errorBuilder: (context, state) => const ErrorPage(),
    routes: [
      Routes.home,
      Routes.pokemon,
    ],
  );
}
