import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import 'package:pokedex/core/util/util_core.dart';
import 'package:pokedex/modules/home/home_module.dart';

import '../poke_info_module.dart';

class PokeInfoPage extends StatefulWidget {
  const PokeInfoPage({super.key});

  @override
  State<PokeInfoPage> createState() => _PokeInfoPageState();
}

class _PokeInfoPageState extends State<PokeInfoPage> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Scaffold(
          appBar: AppBar(
            leading: (constraints.maxWidth > 600)
                ? null
                : IconButton(
                    onPressed: () => GoRouter.of(context).go('/'),
                    icon: const Icon(Icons.arrow_back),
                  ),
          ),
          body: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (constraints.maxWidth > 600) ...[
                  const Expanded(child: HomeProvider()),
                ],
                Expanded(
                  flex: 2,
                  child: BlocConsumer<PokeInfoBloc, PokeInfoState>(
                    listener: (context, state) => Logger.info.log(state),
                    builder: (context, state) {
                      if (state is PokeInfoFetching) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (state is PokeInfoFetchSuccess) {
                        return Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(state.data.id.toString()),
                              Text(state.data.name),
                              Image.network(
                                state.data.spriteUrl,
                                fit: BoxFit.cover,
                                width: 150,
                                height: 150,
                              ),
                            ],
                          ),
                        );
                      } else {
                        return const Center(child: Text('error'));
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
