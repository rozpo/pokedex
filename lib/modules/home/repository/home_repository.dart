import 'dart:convert';

import 'package:pokedex/core/api/api_controller.dart';
import 'package:pokedex/core/cache/cache_controller.dart';
import 'package:pokedex/core/util/util_core.dart';
import 'package:pokedex/core/util/src/logger.dart';
import 'package:pokedex/modules/home/model/home_model.dart';
import 'package:pokedex/modules/home/service/home_service.dart';

mixin HomeRepository {
  static Future<HomeModel?> fetchPokemonList({
    int offset = 0,
    int limit = 0,
  }) async {
    var uri = ApiController.pokemon(offset: offset - 1, limit: limit);
    String? request = CacheController.getRequest(uri.toString());

    if (request == null) {
      Logger.debug.log('no cache for endpoint $uri');
      final result = await HomeService.getPokemonList(uri);

      if (result.statusCode == 200) {
        Logger.debug.log('request status code: ${result.statusCode}');
        request = result.body;
        await CacheController.saveRequest(uri.toString(), request);
      } else {
        Logger.error.log('request error code: ${result.statusCode}');
      }
    } else {
      Logger.debug.log('using cache for endpoint $uri');
    }

    return HomeModel.fromJson(jsonDecode(request!));
  }
}
