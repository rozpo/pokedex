enum Sizer {
  x1(8),
  x2(16),
  x3(24),
  x4(32),
  x5(40),
  x6(48),
  x7(56),
  x8(64);

  final double value;
  const Sizer(this.value);
}
