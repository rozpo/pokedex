import 'package:shared_preferences/shared_preferences.dart';

mixin CacheController {
  static late final SharedPreferences _prefs;

  static Future<void> init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  static Future<void> saveRequest(String key, String value) async {
    await _prefs.setString(key, value);
  }

  static String? getRequest(String key) {
    return _prefs.getString(key);
  }
}
