import 'dart:developer' as developer;

import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:stack_trace/stack_trace.dart';

enum Logger {
  verbose(0),
  debug(500),
  info(1000),
  warning(1250),
  error(1500),
  fatal(2000);

  static int _level = Logger.debug.level;
  static set setLogLevel(int level) => _level = level;

  static void setDefaultLogLevel() => kDebugMode
      ? setLogLevel = Logger.debug.level
      : setLogLevel = Logger.info.level;

  final int level;
  const Logger(
    this.level,
  );

  void log(Object message) {
    if (level >= Logger._level) {
      String name = '${this.name.toUpperCase()}]'.padRight(8);
      String time = DateFormat('[yyyy-MM-dd|kk:mm:ss').format(DateTime.now());
      String location = Frame.caller().location;

      developer.log(
        message.toString(),
        name: '$name $time $location',
        time: DateTime.now(),
        level: level,
      );
    }
  }
}
