import 'package:go_router/go_router.dart';

import 'package:pokedex/modules/home/home_module.dart';
import 'package:pokedex/modules/poke_info/poke_info_module.dart';

mixin Routes {
  static RouteBase home = GoRoute(
    path: '/',
    builder: (context, state) => const HomeProvider(),
  );

  static RouteBase pokemon = GoRoute(
    path: '/pokemon/:id',
    builder: (context, state) =>
        PokeInfoProvider(id: int.tryParse(state.params['id']!) ?? 0),
  );
}
